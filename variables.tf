variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "CDN name"
}

variable "domain" {
  type        = string
  description = "Domain name"
}

variable "subdomain" {
  type        = string
  description = "Subdomain name"
}

variable "lb_domain" {
  type        = string
  description = "Application Load Balancer domain name"
}

variable "subdirectory" {
  type        = string
  description = "Subdirectory name"
  default     = ""
}

variable "viewer_protocol_policy" {
  type        = string
  description = "Viewer Protocol Policy"
  default     = "redirect-to-https"
}

variable "cache_enabled" {
  type        = bool
  description = "Enable or no CDN cache"
  default     = false
}

variable "cache_behaviors" {
  type = list(object({
    path        = string
    min_ttl     = number
    default_ttl = number
    max_ttl     = number
  }))
  description   = "Custom Cache Behavior"
  default       = []
}

variable "create_route53_record" {
  type        = bool
  description = "Create route 53 record"
  default     = true
}
