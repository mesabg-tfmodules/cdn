terraform {
  required_version = ">= 0.13.2"
}

data "aws_acm_certificate" "certificate" {
  domain      = var.domain
  types       = ["AMAZON_ISSUED"]
  statuses    = ["ISSUED"]
  most_recent = true
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  origin {
    domain_name = var.lb_domain
    origin_id   = "LB-${var.subdomain}"

    custom_origin_config {
      http_port                 = 80
      https_port                = 443
      origin_protocol_policy    = "https-only"
      origin_ssl_protocols      = ["TLSv1"]
    }
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = "LB-${var.subdomain}"

    forwarded_values {
      query_string = true
      headers      = [
        "Origin",
        "Accept",
        "Accept-Encoding",
        "Accept-Language",
        "Access-Control-Request-Headers",
        "Access-Control-Request-Method",
        "Authorization",
        "Referer",
        "Cache-Control"
      ]

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = var.cache_enabled ? 3600 : 0
    max_ttl                = var.cache_enabled ? 86400 : 0
    compress               = true
    viewer_protocol_policy = var.viewer_protocol_policy
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.cache_behaviors
    iterator = behavior
    content {
      path_pattern     = behavior.value.path
      allowed_methods  = ["GET", "HEAD", "OPTIONS"]
      cached_methods   = ["GET", "HEAD", "OPTIONS"]
      target_origin_id = "LB-${var.subdomain}"

      forwarded_values {
        query_string = true
        headers      = [
          "Origin",
          "Accept",
          "Accept-Encoding",
          "Accept-Language",
          "Access-Control-Request-Headers",
          "Access-Control-Request-Method",
          "Authorization",
          "Referer",
          "Cache-Control"
        ]

        cookies {
          forward = "none"
        }
      }

      min_ttl                = behavior.value.min_ttl
      default_ttl            = behavior.value.default_ttl
      max_ttl                = behavior.value.max_ttl
      compress               = true
      viewer_protocol_policy = var.viewer_protocol_policy
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = var.name
  default_root_object = "${var.subdirectory != "" ? "${var.subdirectory}/" : ""}index.html"
  http_version        = "http2"
  wait_for_deployment = true

  aliases = [var.subdomain]

  price_class = "PriceClass_All"

  viewer_certificate {
    acm_certificate_arn             = data.aws_acm_certificate.certificate.arn
    cloudfront_default_certificate  = false
    minimum_protocol_version        = "TLSv1.1_2016"
    ssl_support_method              = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type      = "none"
    }
  }

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}
