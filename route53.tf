data "aws_route53_zone" "zone" {
  name = var.domain
}

resource "aws_route53_record" "route53_record" {
  count                     = var.create_route53_record ? 1 : 0
  zone_id                   = data.aws_route53_zone.zone.zone_id
  name                      = var.subdomain
  type                      = "A"

  alias {
    name                    = aws_cloudfront_distribution.cloudfront_distribution.domain_name
    zone_id                 = aws_cloudfront_distribution.cloudfront_distribution.hosted_zone_id
    evaluate_target_health  = false
  }
}
