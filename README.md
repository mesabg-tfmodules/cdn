# ELB CDN Module

This module is capable to generate a CDN cloudfront distribution and link it to an existing Application Load Balancer.
NOTE: Certificate MUST be always present on us-east-1 region.

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general CDN name
- `domain` - base domain
- `subdomain` - subdomain name
- `subdirectory` - subdirectory name (default "")
- `viewer_protocol_policy` - protocol policy (default redirect-to-https)
- `cache_enabled` - enable or not cache enabled (default false)
- `cache_behaviors` - multiple path match cache behaviors

Usage
-----

```hcl
module "elb_cdn" {
  source                  = "git::https://gitlab.com/mesabg-tfmodules/cdn.git"

  environment             = "environment"

  name                    = "cdn name"

  domain                  = "foo.com"
  subdomain               = "bar.foo.com"
  lb_domain               = "lb.domain.com"
  subdirectory            = "/some/path"
  viewer_protocol_policy  = "redirect-to-https"

  cache_enabled           = true
  cache_behaviors         = [
    {
      path                = "/assets/*"
      min_ttl             = 0
      default_ttl         = 86400
      max_ttl             = 31536000
    },
  ]
}
```

Outputs
=======

 - `cdn` - Cloudfront distribution
 - `record` - Route53 record


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>

